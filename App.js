import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Button,
  SafeAreaView,
  Image,
  Alert,
} from "react-native";
import { Camera } from "expo-camera";
import axios from "axios";

export default function App() {
  const [hasPermission, setHasPermission] = useState(null);
  // const [pictureUrl, setPictureUrl] = useState();
  const [randNum, setRandNum] = useState();
  const cameraRef = useRef();
  const photoRef = useRef([]);
  const [loading, setLoading] = useState(false);
  const [gesture, setGesture] = useState();

  const [startCounter, setStartCounter] = useState(false);
  const [counter, setCounter] = useState(5);

  useEffect(() => {
    const num = Math.floor(Math.random() * 6);
    setRandNum(num);
  }, [startCounter]);

  useEffect(() => {
    if (startCounter) {
      counter > 0 && setTimeout(() => setCounter(counter - 1), 1000);
    }
  }, [startCounter, counter]);

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);

  if (counter === 0) {
    setCounter(false);
  }

  const chooseGesture = () => {
    switch (randNum) {
      case 0:
        setGesture("left-eye closed, mouth closed, right eye close");
        break;
      case 1:
        setGesture("left-eye closed, mouth closed, right eye open");
        break;
      case 2:
        setGesture("left-eye closed, mouth open, right eye close");
        break;
      case 3:
        setGesture("left-eye closed, mouth open, right eye open");
        break;
      case 4:
        setGesture("left-eye open, mouth closed, right eye closed");
        break;
      case 5:
        setGesture("left-eye open, mouth closed, right eye open");
        break;
      case 6:
        setGesture("left-eye open, mouth open, right eye closed");
        break;
      case 7:
        setGesture("left-eye open, mouth open, right eye open");
        break;
      default:
        setGesture("error");
    }
  };

  const startLiveness = () => {
    chooseGesture();
    setLoading(true);
    setStartCounter(true);
    setCounter(5);

    setTimeout(() => {
      photoRef.current = [];
      capture();
    }, 5000);
  };

  const randomNum = () => {};

  const capture = (counter = 0) => {
    setTimeout(() => {
      if (counter < 8) {
        getImage();
        capture(counter + 1);
      } else {
        // console.log(photoRef.current);

        uploadData();
        setLoading(false);
        setGesture("loading...");
      }
    }, 500);
  };

  const getImage = async () => {
    let photo = await cameraRef.current.takePictureAsync({ quality: 0.6 });
    photoRef.current.push(photo);
    // console.log(photo);
    // setCameraOn(false);
    // setPictureUrl(photo.uri);
  };

  const uploadData = () => {
    const formData = new FormData();
    formData.append("gestures_set", randNum);
    photoRef.current.forEach((photo, index) => {
      formData.append("file", {
        type: "image/png",
        name: `${index}.png`,
        ...photo,
      });
    });

    console.log(formData);

    axios
      .post(
        "https://api.asliri.id:8443/internal_internship_poc/liveness",
        formData,
        {
          headers: {
            token: "NmU0MjhiNTYtZmYzYy00MTdhLWI4M2EtODRiNTJmOTM5NDZh",
            "Content-Type": "multipart/form-data",
          },
        }
      )
      .then((result) => {
        Alert.alert(
          "Response",
          result.data.data.passed ? "results : True" : "Result : False",
          [{ text: "OK", onPress: () => console.log("OK Pressed") }]
        );
        setGesture(false);
        // console.log(result.data.data.passed);
      })
      .catch((err) => {
        setGesture(false);
        console.log(err.response.data);
      });
  };

  if (hasPermission === null) {
    return <View />;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }
  return (
    <SafeAreaView style={styles.droidSafeArea}>
      <Camera
        ratio="16:9"
        ref={cameraRef}
        style={styles.camera}
        type={Camera.Constants.Type.front}
      >
        {gesture && (
          <View style={styles.gestureContainer}>
            <Text style={styles.gesture}>{gesture}</Text>
            <Text style={styles.gesture}> {counter}</Text>
          </View>
        )}
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.button} onPress={startLiveness}>
            {!loading && <Text style={styles.text}> start </Text>}
            {loading && <Text style={styles.text}> loading </Text>}
          </TouchableOpacity>
        </View>
      </Camera>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  droidSafeArea: {
    flex: 1,

    paddingTop: Platform.OS === "android" ? 25 : 0,
  },

  container: {
    flex: 1,
  },
  camera: {
    flex: 1,
  },
  buttonContainer: {
    flex: 1,
    width: 1000,
    backgroundColor: "transparent",
    flexDirection: "row",
    margin: 20,
  },
  button: {
    flex: 0.1,
    alignSelf: "flex-end",
  },
  text: {
    fontSize: 18,
    color: "white",
  },

  gestureContainer: {},

  gesture: {
    textAlign: "center",
    fontSize: 18,
    color: "black",
  },
});
